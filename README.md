# Dub of War #
A fierce tug of war is raging but you are running out of knights who'd fight for you!

Made at Global Game Jam 2016.
https://globalgamejam.org/2016/games/dub-war

# Gameplay #
You need to quickly dub more knight by gently tapping their shoulders with your sword. Try not to lob of any limbs while doing it.

For 2 players (local).
