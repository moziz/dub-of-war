﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class HorizontObject : MonoBehaviour {

	public HorizontMaster master = null;
	public Vector2 position = Vector2.zero;
	public bool destoyIfTooFar = false;
	public float ownScale = 1;
	//Master updates:
	public float distance = 0;

	public void Update()
	{
		if(master == null){
			master = GetComponentInParent<HorizontMaster>();
			if(master == null){
				Debug.LogError("Parent of horizont object should have horizont master!");
				return;
			}
		}
		else{
			distance = master.UpdateTransformToWorld(transform, position,ownScale);
			if(destoyIfTooFar && distance < master.maxNegativeDistance){
				Destroy(gameObject);
			}
		}
	}
}
