﻿using UnityEngine;
using System.Collections;

public class HorizontCollider : MonoBehaviour {

	public float activeDistance = 1;
	public float deactiveDistance = -1;
	public TapCollider collider2d;
	public HorizontObject ho;

    bool collidersEnabled = false;

	void Start(){
		if(collider2d == null){
			collider2d = GetComponent<TapCollider>();
		}
		if(ho==null)
		{
			ho = GetComponentInParent<HorizontObject>();
		}

	}
	
	// Update is called once per frame
	void LateUpdate () {
        if ((ho.distance < activeDistance && ho.distance > deactiveDistance) != collidersEnabled)
        {
            collidersEnabled = !collidersEnabled;
            if (collidersEnabled)
            {
                BroadcastMessage("MakeTapColliders");
            }
            else
            {
                BroadcastMessage("RemoveTapColliders");
            }
        }
	}
}
