﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class HorizontLine : HorizontObject {

	public Text distanceLabel;
	public float distanceValue;
	
	// Update is called once per frame
	new void Update () {
		base.Update();
		if(base.position.y != distanceValue){
			UpdateLabel();
		}
		transform.localScale = new Vector3(1,0.02f,1);
		this.distanceLabel.transform.position = transform.position;
 	}

	public void UpdateLabel()
	{
		this.distanceValue = base.position.y;
		this.distanceLabel.text = "d: " + distanceValue.ToString("0.0");
	}
}
