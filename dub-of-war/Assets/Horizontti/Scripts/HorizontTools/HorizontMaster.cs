﻿using UnityEngine;
using System.Collections;

public class HorizontMaster : MonoBehaviour {

	//Min line is a players pos
	public Transform horizontLine, MinLine;
	public float distanceToHorizont = 100;
	public float distanceWithFormula = 8;
	public float cameraDistanceFromPlayer = 1;
	//public int player = 1;
	public float masterScale = 0.5f;
	public float maxX = 50;
	public float offsetY = 0;

	public float xAngle = 1 / Mathf.Sqrt(3);

	public float maxNegativeDistance = -100;

	//Player
	public float playerYPosition{
		get{
			return playerDebugPosY;
		}
	}

	public float playerDebugPosY = 0;

	//Returns distance
	public float UpdateTransformToWorld(Transform tf, Vector2 position, float ownScale)
	{
		float distance = position.y - playerYPosition;
		float t = GetT(distance);
		float y = GetY(t);
		float x = GetX(position.x, t);
		tf.position = new Vector3(x, y, (y-offsetY) * maxX + x );
		tf.localScale = GetScale(t) * ownScale;
		return distance;
	}

	public Vector3 GetScale(float t)
	{
		if(float.IsNaN(t) || float.IsInfinity(t)){
			return Vector3.one;
		}
		return Vector3.one * masterScale * t;
	}

	public float GetX(float localX, float t)
	{
		return localX * (t * xAngle);
	}

	public float GetT(float distance)
	{
		//TODO Too far away:
		if(distance > distanceToHorizont){
			distance = distanceToHorizont;
		}

		//TODO Too close!
		if(distance == 0){
			distance = 0.0001f;
		}

		float t2 = 8 * (distance/distanceToHorizont);
		float t = Mathf.Pow(2, t2) / Mathf.Pow(3, t2);

		return t;
	}

	public float GetY(float t){
		return horizontLine.position.y * (1-t) + MinLine.position.y * t;
	}
}
