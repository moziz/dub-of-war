﻿using UnityEngine;
using UnityEditor;
using System.Collections;


[CustomEditor(typeof(KnightSpawner))]
public class KnightSpawnerInspector : Editor {

	public override void OnInspectorGUI ()
	{
		if(GUILayout.Button("Delete all")){
			(target as KnightSpawner).DeleteAll();
		}
		base.OnInspectorGUI ();
	}
}
