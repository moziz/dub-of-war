﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(HorizontLine))]
[CanEditMultipleObjects]
public class HorizontLineInspector : Editor {
	public override void OnInspectorGUI ()
	{
		if(GUILayout.Button("Force update Label")){
			for( int i = 0; i < targets.Length; ++i){
				(targets[i] as HorizontLine).UpdateLabel();	
			}
		}

		base.OnInspectorGUI ();
	}
}
