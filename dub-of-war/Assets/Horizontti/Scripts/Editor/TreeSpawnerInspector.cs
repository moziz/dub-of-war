﻿using UnityEngine;
using UnityEditor;
using System.Collections;


[CustomEditor(typeof(TreeSpawner))]
public class TreeSpawnerInspector : Editor {

	public override void OnInspectorGUI ()
	{
		if(GUILayout.Button("Re suffle")){
			(target as TreeSpawner).ReSuffle();
		}
		base.OnInspectorGUI ();
	}
}
