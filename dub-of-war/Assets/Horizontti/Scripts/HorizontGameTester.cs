﻿using UnityEngine;
using System.Collections;

public class HorizontGameTester : MonoBehaviour 
{
	public HorizontMaster horizontti;
	public KnightSpawner knights;
	public float moveSpeed = 10;
	public int player = 1;
	bool moving = false;
	public float swordingDistance = -0.5f;

	public Transform walkerObject;
	public float minWalk = 0, maxWalk = 1;
	public float walkInterval = 1;

	private Knight lastK = null;

	void Start()
	{
		horizontti.playerDebugPosY = 0;
		TreeSpawner[] spawners = FindObjectsOfType<TreeSpawner>();

		for(int i= 0; i < spawners.Length; ++i){
			spawners[i].ReSuffle();
		}

		knights.DeleteAll();
		moving = true;
	}

	void LateUpdate()
	{
		if(moving){
			horizontti.playerDebugPosY += Time.deltaTime * moveSpeed;
			//walk
			walkerObject.localPosition = new Vector3(0, minWalk + Mathf.Sin(horizontti.playerYPosition * walkInterval) * (maxWalk - minWalk)); 

			Knight k = knights.GetClosestKnight();
			if(k!= null && k.distance < swordingDistance)
			{
				moving = false;
				k.BroadcastMessage("MakeTapColliders");
                k.BroadcastMessage("SetSeverEnabled", true);
				k.ActivateColliders();
                k.stopped = true;
				lastK = k;
			}
		}
	}

	void KnightingCompleted(object[] args)
	{
        int playerNumber = (int)args[0];

        //Debug.Log("KnightingCompleted! " + playerNumber);
        if (this.player == playerNumber)
		{
			moving = true;
			if(lastK != null){
				lastK.DeActiveColliders();
			}
		}
	}

}

