﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class TreeSpawner : MonoBehaviour {

	public int amount = 0;
	public float minX = 10, maxX = 50, minY = -20, maxY = 100;
	[HideInInspector]
	public List<HorizontObject> trees = new List<HorizontObject>();
	public GameObject treePrefab;

	public HorizontMaster master;

	public float treeSizeNormal = 0.9f;
	public float treeSizeExtraRandom = 0.2f;

	void Start()
	{
		while(trees.Count < amount){
			InstantiateNewTree();
		}
		for(int i = 0; i < trees.Count; ++i){
			trees[i].position = new Vector2(Random.Range(minX, maxX), Random.Range(minY,maxY));
			trees[i].GetComponent<SpriteRenderer>().flipX = Random.value > 0.5f;
			trees[i].ownScale = treeSizeNormal + treeSizeExtraRandom * Random.value;
			trees[i].Update();
		}
	}

	void InstantiateNewTree(){
		GameObject go = Instantiate<GameObject>(treePrefab);
		HorizontObject ho = go.GetComponent<HorizontObject>();
		ho.master = master;
		trees.Add(ho);
		go.transform.SetParent(master.transform);
	}

	void Update()
	{
		if(trees.Count != amount) Start();
		for(int i = 0; i < trees.Count; ++i)
		{
			if(trees[i].distance < minY){
				trees[i].position.y = maxY + master.playerYPosition;
			}
		}
	}

	public void ReSuffle()
	{
		Start();
	}
}
