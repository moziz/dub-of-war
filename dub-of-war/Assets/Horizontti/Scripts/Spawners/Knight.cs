﻿using UnityEngine;
using System.Collections;

public class Knight : HorizontObject {
	
	public bool stopped = false;

	public void ActivateColliders(){
		Collider2D[] colls = GetComponentsInChildren<Collider2D>();
		for(int i = 0; i < colls.Length; ++i)
		{
			colls[i].enabled= true;
		}
		
	}

	public void DeActiveColliders(){
		Collider2D[] colls = GetComponentsInChildren<Collider2D>();
		for(int i = 0; i < colls.Length; ++i)
		{
			colls[i].enabled= false;
		}

	}

}
