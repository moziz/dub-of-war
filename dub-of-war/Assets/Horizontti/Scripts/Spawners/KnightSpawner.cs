﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class KnightSpawner : MonoBehaviour {

	public float lastSpawn = 40;
	public float interval = 25;
	public float randomInterval = 10;
	public bool left = false;
	public float leftX = -3, rightX = 3, maxDistanceY = 100;
	public float randomX = 2;

	public List<Knight> knights = new List<Knight>();
	public GameObject knightPrefab;

	public HorizontMaster master;


	Knight InstantiateNewKnight(){
		GameObject go = Instantiate<GameObject>(knightPrefab);
		Knight ho = go.GetComponent<Knight>();
		ho.master = master;
		knights.Add(ho);
		go.transform.SetParent(master.transform);
		return ho;
	}

	void Update()
	{
		while(lastSpawn < master.playerDebugPosY + maxDistanceY + interval)
		{
			Spawn(lastSpawn + interval);
		}
			
	}

	public void Spawn(float posY)
	{
		float spawnPos = posY;
		lastSpawn = spawnPos;
		Knight ho = InstantiateNewKnight();
		ho.position = new Vector2((left ? leftX : rightX) + Random.value * randomX - Random.value * randomX, spawnPos + Random.value*randomInterval);
		left = !left;
		ho.Update();
	}

	public Knight GetClosestKnight()
	{

		Knight closest = null;
		float closestDistance = 0;
		for(int i = 0; i < knights.Count; ++i)
		{
			if(knights[i] == null)
			{
				knights.RemoveAt(i);
				--i;
				continue;
			}
			if(knights[i].stopped)
			{
				continue;
			}
			float distance = knights[i].distance;
			if(closest == null || closestDistance > distance){
				closest = knights[i];
				closestDistance = distance;
			}
		}
		return closest;
	}

	public void DeleteAll(){
		lastSpawn = 0;
		for(int i = 0; i < knights.Count; ++i){
			if(!Application.isPlaying){
				DestroyImmediate(knights[0]);
			}
			else{
				Destroy(knights[0]);
			}
		}
		knights.Clear();
	}
}
