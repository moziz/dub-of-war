﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class MakeSoldierPartPool {
    //[MenuItem("Dub of War/Make Soldier Part Pool")]
    static SoldierPartPool MakePartPool() {
        SoldierPartPool pool = ScriptableObject.CreateInstance<SoldierPartPool>();
        AssetDatabase.CreateAsset(pool, "Assets/SoldierPartPool.asset");
        AssetDatabase.SaveAssets();

        return pool;
    }

    [MenuItem("Dub of War/Update Soldier Part Pool")]
    static void UpdateSoldierPartPool() {
        // Find parts
        string[] prefabPaths = AssetDatabase.FindAssets("t:Prefab");

        if (prefabPaths.Length < 1) {
            Debug.LogError("No prefabs were found");
            return;
        }

        List<SoldierPart> parts = new List<SoldierPart>();

        for (int i = 0; i < prefabPaths.Length; i++) {
            SoldierPart part = AssetDatabase.LoadAssetAtPath<SoldierPart>(AssetDatabase.GUIDToAssetPath(prefabPaths[i]));

            if (part != null) {
                parts.Add(part);
            }
        }
        
        // Find pool
        string[] poolPaths = AssetDatabase.FindAssets("t:SoldierPartPool");

        SoldierPartPool partPool;
        if (poolPaths.Length < 1) {
            Debug.Log("No Soldier Part Pool found. Making a new one.");
            partPool = MakePartPool();
            return;
        }

        string poolPath = poolPaths[0];

        if (poolPaths.Length > 1) {
            Debug.LogWarning("More than one Soldier Part Pool was found. Updating the one at " + poolPath);
        }

        partPool = AssetDatabase.LoadAssetAtPath<SoldierPartPool>(AssetDatabase.GUIDToAssetPath(poolPath));
        partPool.partPool = new List<SoldierPart>(parts);

        EditorUtility.SetDirty(partPool);
        AssetDatabase.SaveAssets();
    }
}
