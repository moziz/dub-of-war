﻿using UnityEngine;
using System.Collections.Generic;

public class PullerNudge : MonoBehaviour
{
    Vector3 delta;

    void Start()
    {
        delta = Vector3.zero;
    }

    void Update()
    {
        transform.position -= delta;
        delta = new Vector3(Random.Range(-0.01f, 0.01f), Random.Range(-0.02f, 0.02f));
        transform.position += delta;
    }
}
