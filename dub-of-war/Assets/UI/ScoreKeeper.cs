﻿using UnityEngine;
using UnityEngine.UI;
using Holoville.HOTween;
using System.Collections.Generic;
using System;

public class ScoreKeeper : MonoBehaviour
{
    public Transform start;
    public Transform end;
    public Transform knob;
    public float powerChangeSpeed = 0.05f;
    public float powerLossSpeed = 100f;

    public GameObject pullerPrefab;

    int playerCount;
    List<float> playerScores;
    float powerBalance = 0.0f;
    float pullerDistance = 0.3f;
    public AnimationCurve pullerArrivalCurve;

	public float pumpAmount = 0.1f;

	public AnimationCurve tensionCurve;

    class PullerData
    {
        public float power;
        public Transform transform;
    }

    List<List<PullerData>> playersPullers;
    List<Transform> pullerDump;

	public Text player0Win = null;
	public Text player1Win = null;
	public Text restartInfo = null;

    bool gameOver = false;

    void Start()
    {
        pullerDump = new List<Transform>();
        playerCount = FindObjectsOfType<SwordPointer>().Length;
        playerScores = new List<float>();
        playersPullers = new List<List<PullerData>>();
        for (int i = 0; i < playerCount; i++)
        {
            playerScores.Add(0.0f);
            playersPullers.Add(new List<PullerData>());
        }
        powerBalance = 0.0f;

        player0Win.text = "";
        player1Win.text = "";
		restartInfo.text = "";

        HOTween.Init();
    }

    void Update()
    {
        if(gameOver)
        {
            if(Input.GetButtonDown("Restart"))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            }
            return;
        }


        if (playerScores == null || playerScores.Count < 1)
            return;

        for (int i = 0; i < playerScores.Count; i++)
        {
            if(playerScores[i] > 0)
            {
                playerScores[i] -= powerLossSpeed * Time.deltaTime;
                if (playerScores[i] < 0)
                    playerScores[i] = 0;
            }
        }

        for (int i = 0; i < playersPullers.Count; i++)
        {
            playerScores[i] = 0.0f;
            for (int j = 0; j < playersPullers[i].Count; j++)
            {
                playersPullers[i][j].power -= powerLossSpeed * Time.deltaTime;

                if (playersPullers[i][j].power <= 0.0f)
                {
                    SpriteRenderer sr = playersPullers[i][j].transform.GetComponent<SpriteRenderer>();
                    HOTween.To(sr, 0.5f, new TweenParms()
                        .Prop("color", new Color(1, 1, 1, 0))
                        .Ease(EaseType.EaseOutExpo)
                    );

                    HOTween.To(playersPullers[i][j].transform, 0.5f, new TweenParms()
                        .Prop("position", -Vector3.up, true)
                        .Ease(pullerArrivalCurve)
                    );

                    pullerDump.Add(playersPullers[i][j].transform);
                    playersPullers[i].RemoveAt(j);
                    
                    for (int k = j; k < playersPullers[i].Count; k++)
                    {
                        HOTween.To(playersPullers[i][k].transform, 0.3f, new TweenParms()
                            .Prop("position", Vector3.right * pullerDistance * (i == 0 ? 1 : -1) - Vector3.forward * pullerDistance, true)
                            .Ease(pullerArrivalCurve)
                        );
                    }

                    --j;
                    continue;
                }

                playerScores[i] += playersPullers[i][j].power;
            }
        }
        
        float player0Score = playerScores[0];
        float player1Score = playerScores[1];

        if (player0Score > player1Score)
            powerBalance += (player0Score - player1Score) * Time.deltaTime * powerChangeSpeed;
        else
            powerBalance -= (player1Score - player0Score) * Time.deltaTime * powerChangeSpeed;

        if (powerBalance > 1)
        {
            player0Win.text = "Victory!";
			PumpText(player0Win);
            player1Win.text = "Lost!";
			DoGameOver(0);
			restartInfo.text = "Press Space to restart";
			PumpText(restartInfo);
        }
        else if (powerBalance < -1)
        {
            player0Win.text = "Lost!";
			player1Win.text = "Victory!";
			PumpText(player1Win);
			DoGameOver(1);
			restartInfo.text = "Press Space to restart";
			PumpText(restartInfo);
        }

		knob.position = Vector3.Lerp(start.position, end.position, tensionCurve.Evaluate(1.0f - (powerBalance + 1.0f) / 2.0f));


        for (int i = 0; i < pullerDump.Count; i++)
        {
            if(pullerDump[i].GetComponent<SpriteRenderer>().color.a <= 0.01f)
            {
                Destroy(pullerDump[i].gameObject);
                pullerDump.RemoveAt(i);
                --i;
            }
        }

		if(Input.GetKeyUp(KeyCode.Escape)){
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);
		}
    }

	private void PumpText(Text t)
	{
		HOTween.To(t.transform, 0.5f, new TweenParms()
			.Prop("localScale", Vector3.one * pumpAmount, true)
			.Ease(EaseType.EaseInOutSine).Loops(-1, LoopType.Yoyo)
		);
	}

    private void DoGameOver(int playerNumber)
    {
        gameOver = true;

        // Broadcast to every game object in scene
        GameObject[] gos = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
        foreach (GameObject go in gos)
        {
            if (go && go.transform.parent == null)
            {
                go.gameObject.BroadcastMessage("GameOver", playerNumber, SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    void KnightingCompleted(object[] args)
    {
        int playerNumber = (int)args[0];
        float remainingPower = (float)args[1];

        if(remainingPower <= 0.01f)
        {
            return;
        }

        if (playerScores == null)
        {
            playerScores = new List<float>();
        }
        while (playerScores.Count <= playerNumber)
        {
            playerScores.Add(0.0f);
            playerCount = playerScores.Count;
        }
        
        {
            Transform puller = Instantiate(pullerPrefab).transform;
            
            if (playerNumber == 0)
            {
                puller.position = start.position - (Vector3.right - Vector3.forward) * ((1 + playersPullers[playerNumber].Count) * pullerDistance);
            }
            else
            {
                puller.position = end.position + (Vector3.right + Vector3.forward) * ((1 + playersPullers[playerNumber].Count) * pullerDistance);

                // Flip
                Vector3 s = puller.localScale;
                s.x = -s.x;
                puller.localScale = s;
            }
            //Debug.Log("Remaining power: " + remainingPower);

            playersPullers[playerNumber].Add(new PullerData() { power = remainingPower, transform = puller });

            Vector3 endPos = puller.position;
            if(playerNumber == 0)
            {
                puller.position -= Vector3.right*2;
            }
            else
            {
                puller.position += Vector3.right*2;
            }

            SpriteRenderer sr = puller.GetComponent<SpriteRenderer>();
            sr.color = new Color(1,1,1,0.1f);

            HOTween.To(sr, 0.5f, new TweenParms()
                .Prop("color", new Color(1, 1, 1, 1))
                .Ease(EaseType.EaseOutExpo) // Ease
            );

            HOTween.To(puller, 0.5f, new TweenParms()
                .Prop("position", endPos, false) // Position tween (set as relative)
                .Ease(pullerArrivalCurve) // Ease
            );
        }
    }
}
