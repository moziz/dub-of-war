﻿using UnityEngine;
using System.Collections;

public class TapColliderCreator : MonoBehaviour
{
    public GameObject tapColliderPrefab = null;
    GameObject generatedCollider = null;
    
    void MakeTapColliders()
    {
        generatedCollider = Instantiate(tapColliderPrefab);
        generatedCollider.GetComponent<TapCollider>().Init(GetComponentInParent<SoldierTapping>());
        generatedCollider.transform.position = transform.position;
    }

    void RemoveTapColliders()
    {
        if(generatedCollider)
        {
            Destroy(generatedCollider);
            generatedCollider = null;
        }
    }

    void OnDestroy()
    {
        RemoveTapColliders();
    }


    void GameOver(int playerNumber)
    {
        Destroy(gameObject);
    }
}
