﻿using UnityEngine;
using Holoville.HOTween;

public class SwordPointer : MonoBehaviour
{
    public int playerNumber = 0;

    public float speed = 0.5f;
    public float acceleration = 0.2f;
    public float damping = 0.9f;
    public AnimationCurve returnCurve;
	public Transform headLevel;

    Vector3 velocity = Vector3.zero;
    Vector3 returnPoint;
    bool returning = false;
    bool noInput = false;
    bool riseToHeadLevel;
    float headLevelY;
    float headLevelHighlightAlpha = 0.0f;
    float riseToHeadLevelStart;
    float currentSoldierPoints = 1.0f;
    SpriteRenderer headLevelHighlight;

	public bool keyboard = true;

	bool mac = true;
    private bool gameOver = false;

    void Start()
    {
		keyboard = PlayerPrefs.GetInt("keyboard"+playerNumber,1) == 1;

        headLevelY = headLevel.position.y;
        headLevelHighlight = headLevel.GetComponentInChildren<SpriteRenderer>();
        headLevelHighlightAlpha = headLevelHighlight.color.a;
        Color col = headLevelHighlight.color;
        col.a = 0;
        headLevelHighlight.color = col;
        returnPoint = transform.localPosition;
		mac = (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor);
    }

    void Update()
    {
        if (gameOver)
            return;

        if (!noInput)
        {
            // Get player input
			velocity.x = Input.GetAxis("HorizontalPlayer" + (playerNumber + 1) + (keyboard ? "Keyboard" : (mac ? "StickMac" : "StickWin")));
			velocity.y = Input.GetAxis("VerticalPlayer" + (playerNumber + 1) + (keyboard ? "Keyboard" : (mac ? "StickMac" : "StickWin")));

            // Move pointer
            transform.localPosition += 60 * Time.deltaTime * velocity * speed;
        }

        if (returning)
        {
            velocity = Vector3.zero;
            //transform.localPosition = Vector3.Lerp(transform.localPosition, returnPoint, 0.1f);
        }

        if(riseToHeadLevel)
        {
            if(transform.position.y >= headLevelY)
            {
                riseToHeadLevel = false;
                NextKnight();

                Color col = headLevelHighlight.color;
                col.a = 0;
                headLevelHighlight.color = col;
            }
            else if(riseToHeadLevelStart < Time.time)
            {
                Color c = headLevelHighlight.color;
                c.a = Mathf.Lerp(0, headLevelHighlightAlpha, Mathf.PingPong((riseToHeadLevelStart - Time.time) * 5.0f, 1.0f));
                headLevelHighlight.color = c;
            }
        }
    }

    void NextKnight()
    {
        noInput = true;
        returning = true;
        velocity = Vector3.zero;

        HOTween.To(transform, 0.5f, new TweenParms()
            .Prop("position", returnPoint - transform.position, true) // Position tween (set as relative)
            .Ease(returnCurve) // Ease
            .OnStepComplete(KnightReady) // OnComplete callback
        );

        DoCompleteKnighting();
    }

    void KnightReady()
    {
        noInput = false;
        returning = false;
    }
    
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (noInput)
            return;
        if (coll.gameObject.tag == "SoldierPart")
        {
            currentSoldierPoints -= coll.gameObject.GetComponentInParent<SoldierPart>().Sever();
        }
        else if (coll.gameObject.tag == "ShoulderCollider")
        {
            bool done = coll.gameObject.GetComponent<TapCollider>().GetTouched();

            if(done)
            {
                riseToHeadLevel = true;
                riseToHeadLevelStart = Time.time + 1.0f;
            }
        }
    }

    void DoCompleteKnighting()
    {
        // Broadcast to every game object in scene
        GameObject[] gos = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
        foreach (GameObject go in gos)
        {
            if (go && go.transform.parent == null)
            {
                if (currentSoldierPoints < 0)
                    currentSoldierPoints = 0.05f;
                //Debug.Log("Current soldier point: " + currentSoldierPoints);
                go.gameObject.BroadcastMessage("KnightingCompleted", new object[] { playerNumber, currentSoldierPoints }, SendMessageOptions.DontRequireReceiver);
            }
        }
        currentSoldierPoints = 1.0f;
    }

    void GameOver(int playerNumber)
    {
        gameOver = true;
    }
}
