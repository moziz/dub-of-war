﻿using UnityEngine;

public class FollowSwordPointer : MonoBehaviour
{
    Vector3 startPos;
    
    void Start()
    {
        startPos = transform.position;
        transform.localPosition = Vector3.zero;
    }
    
    void Update()
    {
        transform.LookAt(startPos, -Vector3.forward);
    }
}
