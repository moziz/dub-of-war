﻿using UnityEngine;
using System.Collections;

public class SoldierTapping : MonoBehaviour
{
    int shoulderCount = 0;
    bool completed = false;

    void Awake()
    {
        shoulderCount = 0;
        completed = false;
    }
    
    void DoComplete()
    {
        completed = true;
    }

    public void IncrementShoulderCount()
    {
        shoulderCount += 1;
    }
    public bool ShoulderWasTapped()
    {
        shoulderCount -= 1;
        if (shoulderCount == 0 && !completed)
        {
            DoComplete();
            return true;
        }
        return false;
    }
}
