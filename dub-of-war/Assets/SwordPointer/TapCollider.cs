﻿using UnityEngine;
using System.Collections;

public class TapCollider : MonoBehaviour
{
    public SoldierTapping parentSoldier = null;
    private SpriteRenderer spriteRenderer = null;

    void Update()
    {
        if (spriteRenderer == null)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
        
        Color c = spriteRenderer.color;
        c.a = Mathf.PingPong(Time.time*2f, 0.6f);
        spriteRenderer.color = c;
    }

    public void Init(SoldierTapping soldier)
    {
        parentSoldier = soldier;
        parentSoldier.IncrementShoulderCount();
    }

    public bool GetTouched()
    {
        if (!parentSoldier)
            return false;

        bool done = false;
        if (parentSoldier)
        {
            done = parentSoldier.ShoulderWasTapped();
            parentSoldier = null;
        }

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
        return done;
    }
}
