﻿using UnityEngine;
using System.Collections.Generic;

public class SoldierPartSocket : MonoBehaviour {
    public SideVariant sideVariant = SideVariant.none;
    public float socketSize;
    public SoldierPart.PartType defaultAttachmentType;

    public enum SideVariant {
        none,
        left,
        right
    }

    public void AttachDefault(SoldierPartPool partPool, bool recurse) {
        SoldierPart part = Instantiate<SoldierPart>(partPool.InstantiateRandomPartOfType(defaultAttachmentType));
        part.Init(sideVariant);

        var partTrans = part.transform;
        partTrans.SetParent(transform, false);
        partTrans.localPosition = Vector3.zero;
        partTrans.localRotation = Quaternion.identity;
        
        if (recurse) {
            part.PopulateAllSocketsWithDefaultParts(partPool, recurse);
        }
    }
}
