﻿using UnityEngine;
using System.Collections.Generic;

public class SoldierPart : MonoBehaviour {
    public PartType partType;

    public GameObject leftSideVariant;
    public GameObject rightSideVariant;

    public Transform severEffect;
    public bool rigidbodyOnSever = true;

    bool severEnabled = false;
    List<SoldierPartSocket> sockets;

    public enum PartType {
        head,
        ear,
        neck,
        chest,
        legs,
        shoulder,
        arm,
        hand,
        foot,
        eye,
        nose,
        hat,
        mouth,
        weapon
    }

    void FixedUpdate() {
        if (transform.position.y < 25f && transform.position.y > 10f) {
            Destroy(gameObject);
        }
    }

    public void Init(SoldierPartSocket.SideVariant side) {
        // Select side if needed
        if (side != SoldierPartSocket.SideVariant.none) {
            if (side == SoldierPartSocket.SideVariant.left) {
                leftSideVariant.SetActive(true);
                rightSideVariant.SetActive(false);

            } else {
                rightSideVariant.SetActive(true);
                leftSideVariant.SetActive(false);
            }
        }

        // Discover sockets of active side variants
        sockets = new List<SoldierPartSocket>(
            GetComponentsInChildren<SoldierPartSocket>(false)
        );
    }

    public void PopulateAllSocketsWithDefaultParts(SoldierPartPool partPool, bool recurse) {
        if (sockets == null || sockets.Count == 0) {
            return;
        }

        for (int i = 0; i < sockets.Count; i++) {
            SoldierPartSocket socket = sockets[i];

            socket.AttachDefault(partPool, recurse);
        }
    }

    public void SetSeverEnabled(bool enabled) {
        severEnabled = enabled;
    }

    public float Sever() {
        if (!severEnabled) {
            return 0.0f;
        }

        SetSeverEnabled(false);

        if (rigidbodyOnSever) {
            Rigidbody2D physicsBody = gameObject.AddComponent<Rigidbody2D>();
            physicsBody.AddForce(Vector2.up * 10f + 5f * ((Random.value * 2f - 1f) * Vector2.left), ForceMode2D.Impulse);
			physicsBody.AddTorque((Random.value * 2f - 1f) * 10f, ForceMode2D.Impulse);
			transform.SetParent(null);
        }

        if (severEffect != null) {
            Transform effect = Instantiate<Transform>(severEffect);
            effect.SetParent(transform, false);
            effect.localRotation = Quaternion.identity;
        }

		Sounds.instance.RandomScreem();

        switch (partType) {
            case PartType.chest:
            case PartType.head:
                return 1f;

            case PartType.arm:
                return 0.4f;

            case PartType.weapon:
                return 0.3f;

            case PartType.hat:
            case PartType.nose:
            case PartType.ear:
                return 0.1f;

            default:
                return 0.05f;
        }
    }
}
