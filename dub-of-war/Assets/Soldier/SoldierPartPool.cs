﻿using UnityEngine;
using System.Collections.Generic;

public class SoldierPartPool : ScriptableObject {
    public List<SoldierPart> partPool;

    Dictionary<SoldierPart.PartType, List<SoldierPart>> partsByType;

    public SoldierPart InstantiateRandomPartOfType(SoldierPart.PartType partType) {
        CheckPartsByType();

        List<SoldierPart> partsOfType;
        if (!partsByType.TryGetValue(partType, out partsOfType)) {
            Debug.LogError("No parts of type (" + partType + ") found.");
            return null;
        }

        return partsOfType[(int)(partsOfType.Count * Random.value)];
    } 

    void CheckPartsByType() {
        if (partsByType != null) {
            return;
        }

        partsByType = new Dictionary<SoldierPart.PartType, List<SoldierPart>>();

        for (int i = 0; i < partPool.Count; i++) {
            SoldierPart currentPart = partPool[i];
            List<SoldierPart> partsOfSameType;

            if (!partsByType.TryGetValue(currentPart.partType, out partsOfSameType)) {
                partsOfSameType = new List<SoldierPart>(8);
                partsByType.Add(currentPart.partType, partsOfSameType);
            }

            partsOfSameType.Add(currentPart);
        }
    }
}
