﻿using UnityEngine;
using System.Collections;

public class Soldier : MonoBehaviour {
    public SoldierPartPool partPool;

    void Start() {
        SoldierPart legs = Instantiate<SoldierPart>(partPool.InstantiateRandomPartOfType(SoldierPart.PartType.legs));
        legs.transform.SetParent(transform, false);
        legs.Init(SoldierPartSocket.SideVariant.none);
        legs.PopulateAllSocketsWithDefaultParts(partPool, true);

		GetComponent<Knight>().DeActiveColliders();
    }
}
