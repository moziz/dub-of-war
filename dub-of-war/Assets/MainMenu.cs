﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public Image p1KeySelected;
	public Image p1SticSelected;
	public Image p2KeySelected;
	public Image p2SticSelected;

	public GameObject scroll;

	public GameObject canvasObj0;
	public GameObject canvasObj1;
	public GameObject canvasObj2;
	public GameObject canvasObj3;


	public bool starting = false;
	// Use this for initialization
	void Start () {
		scroll.SetActive(false);
		bool keyboardP1 = PlayerPrefs.GetInt("keyboard0",1) == 1;
		bool keyboardP2 = PlayerPrefs.GetInt("keyboard1",1) == 1;
		p1KeySelected.enabled = keyboardP1;
		p1SticSelected.enabled = !keyboardP1;
		p2KeySelected.enabled = keyboardP2;
		p2SticSelected.enabled = !keyboardP2;

	}

	void Update(){
		if(starting && Input.anyKeyDown){
			StartAfterStart();
		}
		if(Input.GetKeyUp(KeyCode.Escape)){
			Application.Quit();
		}
	}

	public void SelectP1Keyboard()
	{
		PlayerPrefs.SetInt("keyboard0",1);
		Start();
	}

	public void SelectP1Stick()
	{
		PlayerPrefs.SetInt("keyboard0",0);
		Start();
	}

	public void SelectP2Keyboard()
	{
		PlayerPrefs.SetInt("keyboard1",1);
		Start();
	}

	public void SelectP2Stick()
	{
		PlayerPrefs.SetInt("keyboard1",0);
		Start();
	}

	public void StartGame()
	{
		scroll.SetActive(true);
		canvasObj0.SetActive(false);
		canvasObj1.SetActive(false);
		canvasObj2.SetActive(false);
		canvasObj3.SetActive(false);
		starting = true;
		Invoke("StartAfterStart", 5);
	}

	private void StartAfterStart(){
		CancelInvoke();
		UnityEngine.SceneManagement.SceneManager.LoadScene(1);
	}
}
