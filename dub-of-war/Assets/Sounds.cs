﻿using UnityEngine;
using System.Collections;

public class Sounds : MonoBehaviour {

	public AudioClip[] screems;
	public AudioSource effect,music;
	float lastSoundTime = 0;
	public float maxSoundInterval = 1;

	public static Sounds instance;

	void Awake(){
		instance = this;
	}

	public void RandomScreem(){
		if(Time.time > lastSoundTime + maxSoundInterval)
		{
			effect.PlayOneShot(screems[Random.Range(0,screems.Length)]);
			lastSoundTime = Time.time;
		}
	}


}
